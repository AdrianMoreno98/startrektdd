import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class StarTrekTDD 
{
	String mapGlobal [][];
	
	int posEnterpriceX;
	int posEnterpriceY;
		
	int nKlington;
	int nStarfleet;
	int nStars;
	
	int nStarDates;
	int nEnergy;
	
	public StarTrekTDD()
	{
		this.mapGlobal = new String [64][64];
		
		for(int i=0 ; i<64 ; i++) //First define the empty universe 
		{
			for(int j=0 ; j<64 ; j++)
			{
				mapGlobal[i][j]=".";
			}
		}
		
		this.posEnterpriceX = 31;
		this.posEnterpriceY = 31;
		this.mapGlobal[this.posEnterpriceX][this.posEnterpriceY] = "<*>"; //Setting the initial position of the EP (center)
		
		this.nKlington = 7;
		this.nStarfleet = 2;
		this.nStars = 20;
		
		this.nStarDates = 30;
		this.nEnergy = 600;
		
		Random r;
		int posX ,posY;
		int i=0;
				
		while(i<this.nKlington) //Random position of the klington
		{
			r = new Random();
			posX = r.nextInt(64);
			posY = r.nextInt(64);
			
			if(this.mapGlobal[posX][posY].equals("."))
			{
				this.mapGlobal[posX][posY] = "+++";
				i++;
			}
		}
		
		i=0;
		
		while(i<this.nStarfleet) //Random position of the start fleet
		{
			r = new Random();
			posX = r.nextInt(64);
			posY = r.nextInt(64);
			
			if(this.mapGlobal[posX][posY].equals("."))
			{
				this.mapGlobal[posX][posY] = ">!<";
				i++;
			}
		}
		
		i=0;
		
		while(i<this.nStars) //Random position of the stars
		{
			r = new Random();
			posX = r.nextInt(64);
			posY = r.nextInt(64);
			
			if(this.mapGlobal[posX][posY].equals("."))
			{
				this.mapGlobal[posX][posY] = "*";
				i++;
			}
		}
	}
	
	public void statusReport() //OK
	{
		System.out.println("\nRemaining energy: " + this.nEnergy);
		System.out.println("Enterprice in quadrant (" + ((this.posEnterpriceX)/8) + "," + ((this.posEnterpriceY)/8) + ") , sector (" + ((this.posEnterpriceX)%8) + "," + ((this.posEnterpriceY)%8) + ")");
		System.out.println("Reaiming stardates: " + this.nStarDates);
	}
	
	public boolean moveEnterprise(int vectorX , int vectorY)
	{
		boolean ret = true; 
		
		int initialQX = this.posEnterpriceX/8;
		int initialQY = this.posEnterpriceY/8;
				
		if(this.posEnterpriceX+vectorX<64 && this.posEnterpriceX>=0 &&this.posEnterpriceY+vectorY<64 && this.posEnterpriceY>=0) //No go outside of the galaxy
		{
			if(this.nEnergy-(Math.abs(vectorX+vectorY))>0) //We check that there is enough energy left
			{
				this.mapGlobal[this.posEnterpriceX][this.posEnterpriceY] = "."; //Update the content of the position
				
				if(this.nStarDates-(Math.abs(initialQX-vectorX/8)+(Math.abs(initialQY-vectorY/8)))<=0 && this.nKlington>0) //We check that there is enough star date left and klington
				{
					ret = false;
					System.out.println("Insufficiente amount of stardates");
				}
				else
				{
					
					if(this.mapGlobal[this.posEnterpriceX+vectorX][this.posEnterpriceY+vectorY].equals(".")) //The position is empty
					{
						this.posEnterpriceX+=vectorX;
						this.posEnterpriceY+=vectorY;
						this.nEnergy -= (Math.abs(vectorX+vectorY));
						this.mapGlobal[this.posEnterpriceX][this.posEnterpriceY] = "<*>";
						this.nStarDates -= (Math.abs(initialQX-this.posEnterpriceX/8)+(Math.abs(initialQY-this.posEnterpriceY/8)));
						
						int i=0;
						int j=0;

						boolean adjacent = false;
						
						while(i<64 && !adjacent)
						{
							j=0;
							
							while(j<64 && !adjacent)
							{
								if(this.mapGlobal[i][j].equals(">!<")) //Find an star fleet
								{
									if((this.posEnterpriceX/8)==(i/8) && (this.posEnterpriceY/8)==(j/8)) //Same quadrant
									{
										if((Math.abs((this.posEnterpriceX%8)-(i%8))==1 && Math.abs((this.posEnterpriceY%8)-(j%8))==0) || Math.abs((this.posEnterpriceX%8)-(i%8))==0 && Math.abs((this.posEnterpriceY%8)-(j%8))==1) //Adjacent sector
										{
											this.nEnergy = 600; //Restore to the max valor of the energy
											adjacent = true;
										}
									}
								}
								
								if(!adjacent)
									j++;
							}
							
							if(!adjacent)
								i++;
						}
					}
					else
					{
						ret = false; 
						System.out.println("The position is not empty");
					}
				}
			}
			else
				System.out.println("Error, insufficient amount of energy to perform the movement");
		}
		else
			System.out.println("Error, the coordinates are not within the galaxy. No move made");

		return ret; 
	}
	
	public int getNEnergy() //OK
	{
		return this.nEnergy;
	}
	
	public void shortRangeSensor() //OK
	{		
		
		int x=7;
		System.out.println();
		
		for(int i=0 ; i<8 ; i++)
		{
			System.out.print("\t" + (i));
		}
		
		System.out.println();
		
		for(int i=0 ; i<8 ; i++)
		{
			System.out.print("\t_");
		}
		
		for(int i=this.posEnterpriceY-((this.posEnterpriceY)%8)+7 ; i>=this.posEnterpriceY-((this.posEnterpriceY)%8) ; i--)
		{
			System.out.println();
			System.out.print(x + "|");
			
			for(int j=this.posEnterpriceX-((this.posEnterpriceX)%8) ; j<this.posEnterpriceX-((this.posEnterpriceX)%8)+8 ; j++)
			{
				System.out.print("\t" + this.mapGlobal[j][i]);
			}
			
			System.out.print("\t|" + x);
			x--;
		}
		
		System.out.println();
		
		for(int i=0 ; i<8 ; i++)
		{
			System.out.print("\t_");
		}
		
		System.out.println();
		
		for(int i=0 ; i<8 ; i++)
		{
			System.out.print("\t" + (i));
		}
	}
	
	public static void main(String []args) throws IOException
	{
		Scanner keyboard = new Scanner(System.in);
		int opt; 
		StarTrekTDD st = new StarTrekTDD();
		
		System.out.println("**** STAR TREK 1977 (REMASTERED) **** © Adrián Moreno Monterde\n");
			
		do
		{
			st.statusReport();
			
			System.out.println("\n0.- Manoeuvring of Enterprise");
			System.out.println("1.- Scanning the galaxy --> Short-range sensor scan");
			System.out.println("2.- Exit game");
			System.out.print("Choose an option: ");
			opt = keyboard.nextInt();
			
			switch(opt)
			{
				case 0:
					int vectorX, vectorY;
					
					System.out.println("X axis movement: ");
					vectorX = keyboard.nextInt();
					
					System.out.println("Y axis movement: ");
					vectorY = keyboard.nextInt();
					
					st.moveEnterprise(vectorX, vectorY);
					
				break;
				
				case 1:					
					st.shortRangeSensor();
					
					//System.out.println("\n\nPress enter to continue");
					//System.in.read();
				break;
				
				case 2:
				break;
				
				default:
				{
					System.out.println("\nIncorrect option...");
					System.out.println("Press enter to continue");
					System.in.read();
				}
			}
			
			
		}while(opt!=2);
		
		System.out.println("\nEnding the programm");
		System.out.println("Press enter to finalize ...");
		System.in.read();
		
		keyboard.close();
	}
}
